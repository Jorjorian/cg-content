# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:33:40 2017

@author: ajorjorian
"""
import simu_reads as sm
from multiprocessing import Process
import sys
import numpy as np
from pathlib import Path


def error_fastqs(l, ref, errors, pos_mat, mask_left, mask_right, left, right,
                 positions, Destination):
    # generates SE reads
    num_reads = int(sys.argv[4])
    c = int(sys.argv[3])
    # generate a standard fq template for all reads with this length
    Template = sm.Fastq_Template(num_reads)
    #get the list of positions to sample from
    mask_left[left] = ~mask_left[left]
    mask_right[right] = ~mask_right[right]
    pos_left = positions[mask_left]
    pos_right = positions[mask_right]
    if c > 0:
        # permute pos mat for readwise error positions
        pos_mat = sm.permute_rows(pos_mat[:num_reads, :l])
        # sample num_reads reads of l length
        Seqs, pos = sm.Get_Reads_Fast(l, num_reads, ref, pos_left, pos_right)
        for e in errors:
            # loop through error range and add errors to the reads if the fastq 
            # have yet to be generated, the print to fastq
            fastq = (Destination + '/Align_' + str(l) + '_Bases_' + str(e) +
                     '_Errors_' + str(num_reads) + '_Reads.fq.gz')
            file = Path(fastq)
            if (e <= len(Seqs[0, :])) and not file.is_file():
                S, error = sm.Read_Errors(Seqs, e, pos_mat)
                sm.To_Fastq(S, Template, pos, fastq)
                print(fastq + " Done")
    else:
        # where num errors is 0 this branch is activated
        e = 0
        fastq = (Destination + '/Align_' + str(l) + '_Bases_' + str(e) +
                 '_Errors_' + str(num_reads) + '_Reads.fq.gz')
        file = Path(fastq)
        if not file.is_file():
            pos_mat = 0
            Seqs, pos = sm.Get_Reads_Fast(l, num_reads, ref,
                                          pos_left, pos_right)
            S, errors = sm.Read_Errors(Seqs, e, pos_mat)
            sm.To_Fastq(S, Template, pos, fastq)
            print(fastq + " Done")


def main():
    # c is max errors, could probably change this code
    c = int(sys.argv[3])
    errors = np.arange(0, c+1)
    # A and B are the min and max read lengths to generate
    A = [eval(sys.argv[1])]
    if len(A) == 1:
        A = int(sys.argv[1])
        B = int(sys.argv[2]) + 1
        lengths = list(np.arange(A, B))
    else:
        lengths = eval(sys.argv[1])
    # species and dest allow us to specify the reference and destination folders
    species = str(sys.argv[5])
    Dest = str(sys.argv[6])
    # create corresponding mask file name
    name = (Dest + species + "_Masks_" + str(int(min(lengths))) + "_" +
            str(int(max(lengths))) + ".npy")
    # load reference and pos_mat spacer matrix
    ref, pos_mat = sm.processing(Dest, species, testing_mode=False)
    print("Parsing ref")
    # generate mask for allowable sampling locations
    Masks = sm.Parse_Reference_Fast(ref, lengths, species, name, Dest)
    print("Preping Masks")
    Mask_Base_Left = Masks[0][0]
    Left_Permutations = Masks[0][1:]
    Mask_Base_Right = Masks[1][0]
    Right_Permutations = Masks[1][1:]
    #pre generate positiosn span file to save processing
    positions = np.arange(len(ref))
    print("Starting Read Generation")
    i = 0
    # chunks allow for multiprocessing 
    lengths = list(lengths)
    chunksize = int(sys.argv[7])
    chunk_num = int(len(lengths)/chunksize)
    for chunk in range(chunk_num):
        # run through loop and start the read gen process as chuncks of length
        # spans, we could optimize so that reads of length l of different error
        # numbers are not generated sequentially, but memory might be an issue
        # with more than 30 proccesses 
        procs = []
        for value in range(chunksize):
            try:
                l = lengths.pop(0)
                p = Process(target=error_fastqs, args=(l, ref, errors, pos_mat,
                                                       Mask_Base_Left,
                                                       Mask_Base_Right,
                                                       Left_Permutations[i],
                                                       Right_Permutations[i],
                                                       positions, Dest))
                procs.append(p)
                p.start()
                i += 1
            except: 
                print("end")
        for p in procs:
            p.join()


if __name__ == "__main__":
    main()
