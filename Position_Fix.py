# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 11:10:53 2017

@author: ajorjorian
"""
import pandas as pd
import numpy as np
import glob
pos_Files = glob.glob('Positions/*.pos')
B = pd.read_csv('Chr_Len.txt', sep='\t', engine='c', index_col=False,
                header=None)
C = pd.read_csv('chr.txt', sep='\t', engine='c', index_col=False, header=None)
B.iloc[:, 1] = B.iloc[:, 1].str.strip('LN:').astype(float)
B = B.set_index(0)
Reference = [0]
j = 0
for i in C.iloc[:, 0]:
    Reference.append(B.loc[i].values)
    j += 1
Reference = np.array(Reference)
R = Reference.cumsum()
R = R.astype(np.int64)
W = []
for i in pos_Files:
    A = pd.read_csv(i, sep='\t', engine='c', index_col=False,
                    header=None)
    M = []
    for i in range(1, len(R)):
        mask = np.logical_and(A.iloc[:, 0] > R[i-1], A.iloc[:, 0] <= R[i])
        M.append(mask)
    i = 0
    for m in M:
        A.loc[m, 0] = A.loc[m, 0] - R[i] + 1
        i += 1
    E = (A.iloc[:, 0] - A.iloc[:, 1]).abs()
    E = E.where(E < 10).dropna()
    W.append([i, len(E)])
Misalign = pd.DataFrame(W)
Misalign.to_csv("Positions.csv")
