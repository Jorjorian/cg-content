#!/bin/sh
mkdir $2
echo "command $3," >> ${2}Alignment_Timings.txt
for file in $1*.gz; do 
	a=$(basename $file)
	SECONDS=0 ;
	eval  $3 "$file" > ${2}${a%%.*}.sai
	echo "$file,$SECONDS" >> ${2}Alignment_Timings.txt 
done
for file in ${2}*.sai ; do 
	eval $4 "$file" > ${2}${file%%.*}.sam
done
echo "command $3," > ${2}Bwa_Misreads.txt
for file in $2*.sam ; do 
	A=$(samtools view -c -F 260 "$file") 
	echo "${file},${A}" >> ${2}Bwa_Misreads.txt
done 
