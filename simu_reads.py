
import pandas as pd
import glob
import numpy as np
import numpy.random as rand


def processing(Dest, Species='Human', testing_mode=False):
    # positions mat allows sampling positions of reads
    position_mat = np.tile(np.arange(1001), (518000, 1)).astype(np.uint16)
    try:
        # try to load existing reference sequnce
        print("Loading saved reference")
        ref = np.load(Dest + Species+"_reference.npy", mmap_mode='r')
    except IOError:
        print("Files do not exist")
        if Species == 'Human':
            # get all of the preprocessed chrom files
            Refrence_Chroms = glob.glob("Human_Comp/*.fix")
        else:
            Refrence_Chroms = glob.glob("Genome/Ecoli_Processed/*.proc")
        ref = np.array([])
        if len(Refrence_Chroms) > 1:
            for i in range(len(Refrence_Chroms)):
                # import, reshape and drop nans from chromesomes
                print('Processing ' + str(Refrence_Chroms[i]))
                if testing_mode is True:
                    Chr = pd.read_csv(Refrence_Chroms[i], header=None,
                                      index_col=False, engine='c',
                                      nrows=500000)
                else:
                    Chr = pd.read_csv(Refrence_Chroms[i], index_col=False,
                                      header=None, engine='c')
                Chr = Chr.values.flatten().astype(np.int8)
                Chr = Chr[~np.isnan(Chr)]
                ref = np.append(ref, Chr).astype(np.int8)
        else:
            Chr = pd.read_csv(Refrence_Chroms[0], index_col=False,
                              header=None, engine='c')
            Chr = Chr.values.flatten().astype(np.int8)
            Chr = Chr[~np.isnan(Chr)]
            ref = Chr
        # save the assembled reference genome
        ref = ref.astype(np.int8)
        print("saving reference")
        np.save((Dest + Species + "_reference"), ref)
    return ref,  position_mat

def permute_rows(x):
    # allows for row-wise permutation, use this on the pos_mat to get random error locations
    x = x.T
    ix_i = np.random.sample(x.shape).argsort(axis=0)
    ix_j = np.tile(np.arange(x.shape[1]), (x.shape[0], 1))
    return x[ix_i, ix_j].T


def Read_Errors(Seqs, num_errors, position_mat):
    # add errors to set of sampled reads
    positions = np.zeros([len(Seqs), 1])
    if num_errors > 0:
        np.random.seed()
        print("Adding Errors"+ str(num_errors))
        # get the span so each read is edited 
        span = np.arange(len(Seqs)).flatten()
        # permute position_mat and grab the first n positions to choose where to add error
        positions = rand.permutation(position_mat)[:, :num_errors]
        S = np.array(Seqs, dtype=np.int8)
        # choices determines where each error is mapped to in the translator matrix
        choices = rand.choice([0, 1, 2], size=[len(S), num_errors]).astype(np.uint8)
        translator = np.zeros([5, 3]).astype(np.uint8)
        # translator takes base integer as row location choice int as column
        translator[0, :] = [11, 12, 13]
        translator[1, :] = [10, 12, 13]
        translator[2, :] = [10, 11, 13]
        translator[3, :] = [10, 11, 12]
        translator[4, :] = [4, 4, 4]
        # for each error the code below mutates the error positions
        if num_errors > 1:
            for j in range(num_errors):
                S[span[:],
                  positions[:, j].flatten()
                  ] = translator[S[span[:], positions[:, j].flatten()],
                                 choices[:, j].flatten()]
        else:
            S[span, positions.flatten()] = translator[S[span,
                                                      positions.flatten()],
                                                      choices.flatten()]
    else:
        S = np.array(Seqs, dtype=np.int8)
    # translate the errors into sequences and colapse each row into a string
    np.save("TEST_L", S)
    S = pd.DataFrame(S)
    S = S.replace([0, 1, 2, 3, 10, 11, 12, 13, 4],
                  ['A', 'T', 'G', 'C', 'a', 't', 'g', 'c', 'N'])
    S = S.sum(1)
    return S


def Read_Errors_Right(Seqs, num_errors, position_mat):
    # same as Read Errors but with translation altered to be complement
    positions = np.zeros([len(Seqs), 1])
    if num_errors > 0:
        np.random.seed()
        print("Adding Errors")
        span = np.arange(len(Seqs)).flatten()
        positions = rand.permutation(position_mat)[:, :num_errors]
        S = np.array(Seqs, dtype=np.int8)
        choices = rand.choice([0, 1, 2], size=[len(Seqs),
                              num_errors]).astype(np.uint8)
        translator = np.zeros([5, 3]).astype(np.uint8)
        translator[0, :] = [11, 12, 13]
        translator[1, :] = [10, 12, 13]
        translator[2, :] = [10, 11, 13]
        translator[3, :] = [10, 11, 12]
        translator[4, :] = [4, 4, 4]
        if num_errors > 1:
            for j in range(num_errors):
                S[span[:],
                  positions[:, j].flatten()
                  ] = translator[S[span[:], positions[:, j].flatten()],
                                 choices[:, j].flatten()]
        else:
            S[span, positions.flatten()] = translator[S[span,
                                                      positions.flatten()],
                                                      choices.flatten()]
    else:
        S = np.array(Seqs, dtype=np.int8)
    np.save("TEST_R", S)
    S = pd.DataFrame(S)
    S = S.replace([0, 1, 2, 3, 10, 11, 12, 13, 4],
                  ['T', 'A', 'C', 'G', 't', 'a', 'c', 'g', 'N'])
    S = S.sum(1)
    return S


def Get_Reads_Fast(read_length, read_num, ref, pos_left, pos_right):
    # sample reads from reference
    print("getting reads")
    reads = np.ones([read_num, read_length], dtype=np.uint8)*4
    # spacers are allow for vectorized sampling
    if read_length < 200:
        spacer = np.arange(read_length, dtype=np.uint8)
        spacer = np.tile(spacer, ((read_num), 1)).astype(np.uint8)
    else:
        spacer = np.arange(read_length, dtype=np.int16)
        spacer = np.tile(spacer, (read_num, 1)).astype(np.int16)
    np.random.seed()
    # pos_right and pos_left are lists of positions that reads can be sampled
    # from such that they do not intersect NNN regions in the reference
    # this allow for faster sampling as no loops are involved
    # reads are sampled 50% left to right and 50% right to left as to avoid
    # exclusion zones
    positions_r = rand.choice(pos_right, int(read_num/2))
    positions_l = rand.choice(pos_left, int(read_num/2))
    positions_left = np.tile(positions_l,
                             (read_length, 1)).T + spacer[:int(read_num/2)]
    positions_right = np.tile(positions_r,
                              (read_length,
                               1)).T - np.flip(spacer[:int(read_num/2)], 1)
    # positions are recorded so that we can detect false positives
    positions = np.concatenate([positions_l, positions_r - read_length])
    reads[:int(read_num/2), :] = ref[positions_left].astype(np.uint8)
    reads[int(read_num/2):, :] = ref[positions_right].astype(np.uint8)
    reads = reads.astype(np.uint8)
    return reads, positions


def Parse_Reference_Fast(ref, length_range, ref_name, file_name, dest):
    # parse reference figures out which positions we can sample from for each
    # read length such that there wont be NNN overlaps.
    # works correctly with SE but acts strangely for PE
    try:
        masks = np.load(file_name)
    except IOError:
        try:
            Intervals = np.load(dest + "CodingInervals.npy")
        except IOError:
            # grab all N in reference
            A = np.equal(ref, 4)
            B = np.arange(len(A))
            B = B[A]
            Code_Starts = []
            Code_Stops = []
            # tabulate where these NN regions start and stop
            for i in range(1, len(B)-1):
                if not (B[i] == B[i+1]-1):
                    Code_Starts.append(B[i])
                if not (B[i]-1 == B[i-1]):
                    Code_Stops.append(B[i])
            Code_Starts = np.array(Code_Starts)
            Code_Stops = np.array(Code_Stops)
            Intervals = np.zeros([len(Code_Starts), 3]).astype(np.int64)
            Intervals[:, 0] = Code_Starts
            Intervals[:, 1] = Code_Stops
            Intervals[:, 2] = Code_Stops-Code_Starts
            np.save((dest+"CodingInervals.npy"), Intervals)
        # generate masks for the reference span
        mask_left = np.zeros_like(ref).astype(bool)
        mask_right = np.zeros_like(ref).astype(bool)
        left_masks = []
        right_masks = []
        for i in range(len(Intervals)):
            # set long spans of non NN to true
            if Intervals[i, 2] > 1000:
                mask_left[Intervals[i, 0]+1:Intervals[i, 1]-1] = True
                mask_right[Intervals[i, 0]+1:Intervals[i, 1]-1] = True
        left_masks.append(mask_left)
        right_masks.append(mask_right)
        # filp regions in mask corresponding to positions that would lead to
        # read overlapping with NNN to false
        for read_leng in length_range:
            for i in range(len(Intervals)):
                if Intervals[i, 2] > read_leng*2 :
                    mask_left[(Intervals[i, 1]-read_leng):Intervals[i, 1] ] = False
                    mask_right[Intervals[i, 0]:(Intervals[i, 0]+read_leng)] = False
            # save positions where masks for each length differ from reference mask
            left_masks.append(np.where(~np.equal(left_masks[0], mask_left)))
            right_masks.append(np.where(~np.equal(right_masks[0], mask_right)))
        # save masking file
        masks = [left_masks, right_masks]
        name = ( dest + ref_name + "_Masks_" + str(int(min(length_range))) + "_" +
                str(int(max(length_range))) + ".npy")
        np.save(name, masks)
    return masks


def Fastq_Template(leng_seqs):
    # this code sets up a standard template for each fastq
    num_reads = leng_seqs
    IND = np.arange(int(num_reads*4))
    df = pd.DataFrame(np.nan, index=IND, columns=['Read_Info'])
    ID_Positions = np.arange(0, IND[int(num_reads*4)-1], 4)
    Seq_Positions = ID_Positions + 1
    Plus_Positions = Seq_Positions + 1
    Qual_Positions = Plus_Positions + 1
    Template = {"df": df, "Seq_Pos": Seq_Positions, "Qual_Pos": Qual_Positions,
                "ID_Pos": ID_Positions, 'Plus_Pos': Plus_Positions}
    return Template


def To_Fastq(Seqs, Template, positions, Filename):
    # takes input in the form of a series of read strings and read positions
    # adds this data to the fastq template and output fq.gz files
    print("writing fastq")
    df = Template["df"]
    Qual_Positions = Template["Qual_Pos"]
    Seq_Positions = Template["Seq_Pos"]
    ID_Positions = Template["ID_Pos"]
    Plus_Positions = Template["Plus_Pos"]
    positions = list(map(str, positions))
    ID_string = '@Read_'
    IDs = list(map(lambda x: ID_string + x, positions))
    labels = '+'
    df.iloc[Plus_Positions, 0] = labels
    df.iloc[ID_Positions, 0] = IDs
    print(Seqs)
    S = pd.DataFrame(Seqs.values)
    S.index = Seq_Positions[:]
    Quality = ''
    for i in range(len(S.iloc[0, 0])):
        Quality += '~'
    df.iloc[Qual_Positions] = Quality
    df.loc[Seq_Positions] = S
    df.to_csv(Filename,compression='gzip',header=None, index=False)


if __name__ == "__main__":
    main()
