# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:33:40 2017

@author: ajorjorian
"""
import simu_reads as sm
from multiprocessing import Process
import sys
import numpy as np
import pandas as pd
from pathlib import Path


def Dist_Fastqs(Seqs, l, Destination, Template, pos, error, pos_mat):
    num_reads = int(sys.argv[3])
    # permute posmat for error positions
    pos_mat = sm.permute_rows(pos_mat[:num_reads, :l]).astype(int)
    if error > 0:
        for i in range(1, error+1):
            # generate fastq names and skip generation if they exist
            # at the destination 
            fastq = (Destination + 'Pairs_' + str(l)
                     + '_Bases_' + str(i) + '_Errors_' + str(num_reads) +
                     '_Reads.fq.gz')
            file = Path(fastq)
            if not file.is_file():
                print("Splitting")
                # generate a list of insert sizes that seperate read pairs
                Inserts = np.abs(np.round(
                                 np.random.randn(num_reads)*50 +
                                                (350-(2*l))).astype(int))
                # left reads are the first l bases of the overall 800 bp reads
                Read_Left = Seqs[:, :l].astype(np.int8)
                Read_Right = np.zeros_like(Read_Left)
                # right reads are sampled by adding the insert leng to the length 
                # of the first read then extending
                for j in range(num_reads):
                    A = (Seqs[j, (l+Inserts[j]):((2*l)+Inserts[j])])
                    Read_Right[j, :] = A[:]
                # second reads are flipped to get the reverse strand
                Read_Right = np.flip(Read_Right, 1)
                mate = (Destination + 'Pairs_' + str(l) + '_Bases_' + str(i) +
                        '_Errors_' + str(num_reads) + '_Reads.fq.gz.mt')
                # read error functions generate errors and translate 
                Reads_Left = sm.Read_Errors(Read_Left, i, pos_mat)
                Reads_Right = sm.Read_Errors_Right(Read_Right, i, pos_mat)
                # reads are then printed to fastq
                sm.To_Fastq(Reads_Left, Template, pos, fastq)
                sm.To_Fastq(Reads_Right, Template, pos, mate)
                print(fastq + " Done")
    else:
        # the following generates the reads with no errors
        fastq = (Destination + 'Pairs_' + str(l) +
                 '_Bases_0_Errors_' + str(num_reads) + '_Reads.fq.gz')
        file = Path(fastq)
        if not file.is_file():
            print("Splitting")
            Inserts = np.abs(np.round(
                             np.random.randn(num_reads)*50 +
                                            (350-(2*l))).astype(int))
            Read_Left = Seqs[:, :l]
            Read_Right = np.zeros_like(Read_Left)
            for i in range(num_reads):
                A = (Seqs[i, (l+Inserts[i]):((2*l)+Inserts[i])])
                Read_Right[i, :] = A[:]
            Read_Right = np.flip(Read_Right, 1)
            mate = (Destination + 'Pairs_' + str(l) + '_Bases_0_Errors_' +
                    str(num_reads) + '_Reads.fq.gz.mt')
            Reads_Left = sm.Read_Errors(Read_Left, 0, pos_mat)
            Reads_Right = sm.Read_Errors_Right(Read_Right, 0, pos_mat)
            sm.To_Fastq(Reads_Left, Template, pos, fastq)
            sm.To_Fastq(Reads_Right, Template, pos, mate)
            print(fastq + " Done")


def main():
    # get A and B from sysin to set range of read end lengths
    A = [eval(sys.argv[1])]
    if len(A) == 1:
        A = int(sys.argv[1])
        B = int(sys.argv[2]) + 1
        lengths = list(np.arange(A, B))
    else:
        lengths = eval(sys.argv[1])
    # get species, number of errors in the reads, destination folder
    species = str(sys.argv[4])
    Dest = str(sys.argv[5])
    error = int(sys.argv[7])
    # take input info to determine what mask file to look for
    name = (Dest + species + "_Masks_" + str(int(min(lengths))) + "_" +
            str(int(max(lengths))) + ".npy")
    # generate reference and pos_mat 
    ref, pos_mat = sm.processing(Dest, species, testing_mode=False)
    print("Parsing ref")
    # get the masks, in this case we conservatively using settings for 1000bp
    # SE reads that will be sampled and split into smaller pairs
    Masks = sm.Parse_Reference_Fast(ref, [1000, 1001], species, name, Dest)
    print("Preping Masks")
    # grab the long masks
    Mask_Base_Left = Masks[0][0]
    Left_Permutations = Masks[0][1]
    Mask_Base_Right = Masks[1][0]
    Right_Permutations = Masks[1][1]
    positions = np.arange(len(ref))
    print("Starting Read Generation")
    i = 0
    # generate list of read length, multiprocessing chunks
    lengths = list(lengths)
    chunksize = int(sys.argv[6])
    chunk_num = int(len(lengths)/chunksize)
    num_reads = int(sys.argv[3])
    Template = sm.Fastq_Template(num_reads)
    # use masks to get the left and right reading list of acceptable positions 
    Mask_Base_Left[Left_Permutations] = ~Mask_Base_Left[Left_Permutations]
    Mask_Base_Right[Right_Permutations] = ~Mask_Base_Right[Right_Permutations]
    pos_left = positions[Mask_Base_Left]
    pos_right = positions[Mask_Base_Right]
    # sample N 800 bp reads from the reference, 800 so outlier insert sizes 
    # dont fall out of bounds
    Seqs, pos = sm.Get_Reads_Fast(800, num_reads, ref, pos_left, pos_right)
    # the loop below starts a process for each read length for a parralle speed up
    for chunk in range(chunk_num):
        procs = []
        for value in range(chunksize):
            L = lengths.pop(0)
            p = Process(target=Dist_Fastqs, args=(Seqs, L, Dest,
                                                  Template, pos, error,
                                                  pos_mat))
            p.start()
            procs.append(p)
            i += 1
        for p in procs:
            p.join()


if __name__ == "__main__":
    main()
