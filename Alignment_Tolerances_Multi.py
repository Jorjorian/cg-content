# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:33:40 2017

@author: ajorjorian
"""
import simu_reads as sm
from multiprocessing import Pool
import sys
import numpy as np
from pathlib import Path


def error_fastqs(l):
    c = int(sys.argv[3])
    reference = "Human"
    num_reads = int(sys.argv[4])
    Template = sm.Fastq_Template(num_reads+1)
    if c > 0:
        errors = np.arange(1, c+1)
        ref, ref_bounds, coding_leng, pos_mat = sm.processing(reference,
                                                              testing_mode=False)
        pos_mat = sm.permute_rows(pos_mat[:num_reads+1, :l])
        chr_dis = sm.chromosome_distribution(coding_leng, num_reads)
        Seqs = sm.Get_Reads(l, chr_dis, ref_bounds, num_reads, ref)
        for e in errors:
            fastq = ('Tolerance_SE/Align_' + str(l) + '_Bases_' + str(e) +
                     '_Errors_' + str(num_reads) + '_Reads.fq.gz')
            file = Path(fastq)
            if (e <= len(Seqs[0, :])) and not file.is_file():
                S = sm.Read_Errors(Seqs, e, pos_mat)
                sm.To_Fastq(S, Template, fastq)
                print(fastq + " Done")
    else:
        ref, ref_bounds, coding_leng, pos_mat = sm.processing(reference,
                                                              testing_mode=False)
        pos_mat = sm.permute_rows(pos_mat[:num_reads+1, :l])
        chr_dis = sm.chromosome_distribution(coding_leng, num_reads)
        Seqs = sm.Get_Reads(l, chr_dis, ref_bounds, num_reads, ref)
        e = 0
        fastq = ('Tolerance_SE/Align_' + str(l) + '_Bases_' + str(e) +
                 '_Errors_' + str(num_reads) + '_Reads.fq.gz')
        S = sm.Read_Errors(Seqs, e, pos_mat)
        sm.To_Fastq(S, Template, fastq)
        print(fastq + " Done")


def main():
    A = [eval(sys.argv[1])]
    if len(A) == 1:
        A = int(sys.argv[1])
        B = int(sys.argv[2]) + 1
        lengths = list(np.arange(A, B))
    else:
        lengths = eval(sys.argv[1])
    pool = Pool(int(sys.argv[5]))
    pool.map(error_fastqs, lengths)
    pool.close()


if __name__ == "__main__":
    main()
