# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:43:21 2017

@author: ajorjorian
"""

import simu_reads as sm
import pandas as pd
import numpy as np 
import scipy as sp 
import sys
import subprocess as sub
#%% 
reference = "Human"
ref, Test_Mat, ref_bounds, coding_leng = sm.processing(reference,
                                                       testing_mode=False)
num_reads = 10000000