# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:42:05 2017

@author: ajorjorian
"""
import simu_reads as sm
import pandas as pd
import numpy as np 
import scipy as sp 
import sys
import subprocess as sub
#%%
Bin_file = ''
reference = "Human"
ref, ref_bounds, coding_leng = sm.processing(reference,
                                             testing_mode=False)
Bin_Out = []
num_reads = 50000000
chr_dis = sm.chromosome_distribution(coding_leng, num_reads)
seq_lengths = [500, 20]

for i in range(10):
    print('running_cycle ' + str(i))
    Bin_file = "GC_content_calibration_" + str(i) + '.csv'
    Bin_Out.append(sm.CG_Contents_Binning(ref, seq_lengths, chr_dis,
                                          ref_bounds, num_reads, Bin_file,
                                          Save=True))
A = []
B = []
for Bin in Bin_Out:
    A.append(Bin.iloc[0, :])
    B.append(Bin.iloc[len(Bin) - 1, :])
A = np.array(A)
B = np.array(B)
Var20 = A.var(0)
Var500 = B.var(0)

print('20 bp read variation ' + str(Var20))
print('500 bp read variation ' + str(Var500))
