# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 08:36:14 2017

@author: ajorjorian
"""
import pandas as pd
import sys
import numpy as np
filename = str(sys.argv[1])
outname = str(sys.argv[2])
Excel_Name = str(sys.argv[4])
Misalign = str(sys.argv[5])
Uniques = str(sys.argv[6])
Mapq = str(sys.argv[7])
paired = eval(sys.argv[3])

# the if statmemts below import ancillary files tabulating read uniquness 
# and the number of reads mapped to the proper positions
if len(Uniques) > 1:
    Y = pd.read_csv(Uniques, header=None, sep='\t')
    y = Y[0].str.split('_', expand=True).iloc[:, [1, 3]]
    Y_Ind = pd.MultiIndex.from_arrays([y.iloc[:, 0].astype(int),
                                      y.iloc[:, 1].astype(int)],
                                      names=['Bases', 'Errors'])
    Y.index = Y_Ind
    Y = Y.sort_index()
if len(Misalign) > 1:
    Z = pd.read_csv(Misalign, index_col='Unnamed: 0')
    z = Z['Names'].str.split('_', expand=True).iloc[:, [1, 3]]
    Z_Ind = pd.MultiIndex.from_arrays([z.iloc[:, 0].astype(int),
                                      z.iloc[:, 1].astype(int)],
                                      names=['Bases', 'Errors'])
    Z.index = Z_Ind
    Z = Z.sort_index()
# import the concatenated stats.csv file composed of the SN field from 
# samtools stats preceded by each files name
Stats = pd.read_csv(filename, header=None)
# the following extracts error number and read length to create a multiindex 
B = np.arange(0, len(Stats), 32)
Names = Stats.iloc[B, 0].str.split('_', expand=True)
Bases = Names.iloc[:, 1].astype(int)
Index = Bases.astype(int)
Errors = []
if paired is False:
    Errors = Names.iloc[:, 3].astype(int)
    Arrays = [list(Bases), list(Errors)]
    A = list(zip(*Arrays))
    Index = pd.MultiIndex.from_tuples(A, names=['Bases', 'Errors'])
# after setting the index a dataframe is constructed and filled with the 
# various stats values 
Stats_Frame = pd.DataFrame(index=Index)
Stats = Stats.iloc[:, 0].str.split('\t', expand=True)
Stats_Frame['raw total sequences'] = Stats.iloc[B+1, 2].values
Stats_Frame['filtered sequences'] = Stats.iloc[B+2, 2].values
Stats_Frame['sequences'] = Stats.iloc[B+3, 2].values
Stats_Frame['is sorted'] = Stats.iloc[B+4, 2].values
Stats_Frame['1st fragments'] = Stats.iloc[B+5, 2].values
Stats_Frame['last fragments'] = Stats.iloc[B+6, 2].values
Stats_Frame['reads mapped'] = Stats.iloc[B+7, 2].values
Stats_Frame['reads mapped and paired'] = Stats.iloc[B+8, 2].values
Stats_Frame['reads unmapped'] = Stats.iloc[B+9, 2].values
Stats_Frame['reads properly paired'] = Stats.iloc[B+10, 2].values
Stats_Frame['reads paired'] = Stats.iloc[B+11, 2].values
Stats_Frame['reads duplicated'] = Stats.iloc[B+12, 2].values
Stats_Frame['reads MQ0'] = Stats.iloc[B+13, 2].values
Stats_Frame['reads QC failed'] = Stats.iloc[B+14, 2].values
Stats_Frame['non-primary alignments'] = Stats.iloc[B+15, 2].values
Stats_Frame['total length'] = Stats.iloc[B+16, 2].values
Stats_Frame['bases mapped'] = Stats.iloc[B+17, 2].values
Stats_Frame['bases mapped (cigar)'] = Stats.iloc[B+18, 2].values
Stats_Frame['bases trimmed'] = Stats.iloc[B+19, 2].values
Stats_Frame['bases duplicated'] = Stats.iloc[B+20, 2].values
Stats_Frame['mismatches'] = Stats.iloc[B+21, 2].values
Stats_Frame['error rate'] = Stats.iloc[B+22, 2].values
Stats_Frame['average length'] = Stats.iloc[B+23, 2].values
Stats_Frame['maximum length'] = Stats.iloc[B+24, 2].values
Stats_Frame['average quality'] = Stats.iloc[B+25, 2].values
Stats_Frame['insert size average'] = Stats.iloc[B+26, 2].values
Stats_Frame['insert size standard deviation'] = Stats.iloc[B+27, 2].values
Stats_Frame['inward oriented pairs'] = Stats.iloc[B+28, 2].values
Stats_Frame['outward oriented pairs'] = Stats.iloc[B+29, 2].values
Stats_Frame['with other orientation'] = Stats.iloc[B+30, 2].values
Stats_Frame['pairs on different chromosomes'] = Stats.iloc[B+31, 2].values
Stats_Frame = Stats_Frame.astype(float)
Stats_Frame = Stats_Frame.sort_index()
Stats_Frame = Stats_Frame[~Stats_Frame.index.duplicated(keep='first')]
# the supplementary files are then added to the dataframe
Stats_Frame['MapQ > 40'] = Y[1]
Stats_Frame['Multi-Align'] = Y[2]
Stats_Frame['Read 1: Multi-Align'] = Y[3].values
Stats_Frame['Read 2: Multi-Align'] = Y[4].values
Stats_Frame['Read 1: Rescue'] = Y[5].values
Stats_Frame['Read 2: Rescue'] = Y[6].values
Stats_Frame['In Position'] = Z['Mapped In Position']
# dataframe is printed 
Stats_Frame.to_csv(outname, index_label=['Bases','Errors'])
A = Stats_Frame.columns.values
B = {}
# a panel object is generated such that each pane is length vs errors values
# for one of the statistics in the dataframe
if paired is False:
    for i in A:
        W = Stats_Frame[i].unstack()
        B[i] = W
    Z = pd.Panel(data=B)
    Z.to_excel(Excel_Name)
