# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:33:40 2017

@author: ajorjorian
"""
import simu_reads as sm
from multiprocessing import Process
import sys
import numpy as np
from pathlib import Path
import pandas as pd


def error_fastqs(l, ref, errors, pos_mat, mask_left, mask_right, left, right,
                 positions, Destination):
    num_reads = 50000000
    mask_left[left] = ~mask_left[left]
    mask_right[right] = ~mask_right[right]
    pos_left = positions[mask_left]
    pos_right = positions[mask_right]
    pos_mat = sm.permute_rows(pos_mat[:num_reads, :l])
    Seqs, pos = sm.Get_Reads_Fast(l, num_reads, ref, pos_left, pos_right)
    A = (Seqs == 2 | Seqs == 3).sum(1)
    B = np.arange(l+1)
    A = np.histogram(A, bins=B)
    A = pd.DataFrame(A)
    filename = (Destination + str(l) + '_Bases.temp')
    A.to_csv(filename, header=None, index_col=False)


def main():
    c = int(sys.argv[3])
    errors = 0
    A = [eval(sys.argv[1])]
    if len(A) == 1:
        A = int(sys.argv[1])
        B = int(sys.argv[2]) + 1
        lengths = list(np.arange(A, B))
    else:
        lengths = eval(sys.argv[1])
    species = str(sys.argv[5])
    Dest = str(sys.argv[6])
    name = (Dest + species + "_Masks_" + str(int(min(lengths))) + "_" +
            str(int(max(lengths))) + ".npy")
    ref, pos_mat = sm.processing(Dest, species, testing_mode=False)
    print("Parsing ref")
    Masks = sm.Parse_Reference_Fast(ref, lengths, species, name, Dest)
    print("Preping Masks")
    Mask_Base_Left = Masks[0][0]
    Left_Permutations = Masks[0][1:]
    Mask_Base_Right = Masks[1][0]
    Right_Permutations = Masks[1][1:]
    positions = np.arange(len(ref))
    print("Starting Read Generation")
    i = 0
    lengths = list(lengths)
    chunksize = int(sys.argv[7])
    chunk_num = int(len(lengths)/chunksize)
    for chunk in range(chunk_num):
        procs = []
        for value in range(chunksize):
            try:
                l = lengths.pop(0)
                p = Process(target=error_fastqs, args=(l, ref, errors, pos_mat,
                                                       Mask_Base_Left,
                                                       Mask_Base_Right,
                                                       Left_Permutations[i],
                                                       Right_Permutations[i],
                                                       positions, Dest))
                procs.append(p)
                p.start()
                i += 1
            except: 
                print("end")
        for p in procs:
            p.join()
    lengths=np.arange(A,B)
    W=[]
    for l in lengths:
        W.append(pd.read_csv(Dest + str(l) + '_Bases.temp'))
    Matrix=pd.concatenate(W)
    Matrix.to_csv(Species+'_CG_Content')

if __name__ == "__main__":
    main()
