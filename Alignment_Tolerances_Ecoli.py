# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:33:40 2017

@author: ajorjorian
"""
import simu_reads as sm
from multiprocessing import Pool
import sys
import numpy as np
from pathlib import Path


def error_fastqs(l):
    # Get number of errors we want the files to range to
    err = int(sys.argv[3])
    reference = sys.argv[6]  # The reference name
    num_reads = int(sys.argv[4])  # The read number in each fastq
    if err > 0:
        errors = np.arange(1, err+1)
        ref, ref_bounds, coding_leng = sm.processing(reference,
                                                     testing_mode=False)
        Seqs = sm.Get_Reads_One_Chrome(l, num_reads, ref)
        # Seqs is an int8 numpy array with 0-4 corresponding to ATGCN
        for e in errors:
            fastq = ('Ecoli_Tolerance_SE/Align_' + str(l) + '_Bases_' +
                     str(e) + '_Errors.fq.gz')  # Setting Fastq Names
            file = Path(fastq)
            if (e <= len(Seqs[0, :])) and not file.is_file():
                S = sm.Read_Errors(Seqs, e)  # Add errors to the Reads
                # Warning read errors will be on a distribution
                sm.To_Fastq(S,  Template, fastq)
                print(fastq + " Done")
    else:
        ref, ref_bounds, coding_leng = sm.processing(reference,
                                                     testing_mode=False)
        chr_dis = sm.chromosome_distribution(coding_leng, num_reads)
        Seqs = sm.Get_Reads(l, chr_dis, ref_bounds, num_reads, ref)
        e = 0
        fastq = ('Tolerance_SE/Align_' + str(l) + '_Bases_' + str(e) +
                 '_Errors.fq.gz')
        S = sm.Read_Errors(Seqs, e)
        sm.To_Fastq(S,  Template, fastq)
        print(fastq + " Done")


def main():
    # Grabing length range from system input, either a range or a list
    A = [eval(sys.argv[1])]
    if len(A) == 1:
        A = int(sys.argv[1])
        B = int(sys.argv[2]) + 1
        lengths = list(np.arange(A, B))
    else:
        lengths = eval(sys.argv[1])
    # Use pool to create fastqs in parallel
    pool = Pool(int(sys.argv[5]))
    pool.map(error_fastqs, lengths)
    pool.close()


if __name__ == "__main__":
    main()
