# -*- coding: utf-8 -*-
"""
Created on Wed Aug  9 12:33:40 2017

@author: ajorjorian
"""
import simu_reads as sm
import pandas as pd
import numpy as np 
import scipy as sp 
import sys
import subprocess as sub
#%% 
reference = "Human"
ref, Test_Mat, ref_bounds, coding_leng = sm.processing(reference,
                                                       testing_mode=False)
num_reads = 570000
fastq=[]
chr_dis = sm.chromosome_distribution(coding_leng, num_reads)
lengths = np.arange(20, 50)
errors = np.arange(1, 10)
for l in lengths:
    #Seqs = sm.Get_Reads(l, chr_dis, ref_bounds, num_reads, ref)
    for e in errors:
        fastq.append('Align_' + str(l) + '_Bases_' + str(e) + '_Errors.fq.gz')
        #S = sm.Read_Errors(Seqs, e)
        #sm.To_Fastq(S, fastq)
#%% 
calls=[]
for i in fastq: 
    calls.append('bwa mem ref.fa ' + i + ' ' + i.strip('fq.gz') + '.sam' ) 
sub.call()
