#!/bin/sh
mkdir $2
echo "command $3," >> ${2}/Alignment_Timings.txt
for file in $1*.gz; do 
	a=$(basename $file)
	SECONDS=0 ;
	eval  $3 "$file" > ${2}/${a%%.*}.sam
	echo "$file,$SECONDS" >> ${2}/Alignment_Timings.txt 
done 
echo "command $3," > ${2}/Bwa_Misreads.txt
for file in ${2}/*.sam ; do 
	A=$(samtools view -c -F 4  "$file") 
	echo "${file},${A}" >> ${2}/Bwa_Misreads.txt
done 
