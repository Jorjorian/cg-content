# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 13:54:32 2017

@author: ajorjorian
"""
import pandas as pd
import numpy as np
import sys


def main():
    if sys.argv[0] != '':
        filein = sys.argv[1]
        fileout = sys.argv[2]
        read_num = int(sys.argv[3])
        print(filein)
    else:
        filein = "Bwa_Test_Out\Bwa_Misreads.txt"
        fileout = "Misalignment_Table.csv"
        read_num = 1999998
    Misalignments = pd.read_csv(filein, header=None)
    Header = Misalignments.iloc[0,0]
    print(Header)
    Misalignments = Misalignments.iloc[1:]
    Mat_Pos = np.zeros([len(Misalignments), 2])
    for i in range(len(Misalignments)):
        String = Misalignments.iloc[i, 0]
        String = String.split('_')
        String = pd.Series(String)
        String = String[String.str.isnumeric()].astype(int)
        Mat_Pos[i, :] = String.values.astype(int)
    alignments = np.zeros([int(max(Mat_Pos[:, 0]))-18,
                           int(max(Mat_Pos[:, 1]))])
    for i in range(len(Mat_Pos)):
        Bases = int(Mat_Pos[i, 0] - 19)
        Errors = int(Mat_Pos[i, 1])
        alignments[Bases, Errors] = Misalignments.iloc[i, 1]
    Cols = np.arange(Mat_Pos[:, 1].min(),  Mat_Pos[:, 1].max(), dtype=int)
    Rows = np.arange(Mat_Pos[:, 0].min(), Mat_Pos[:, 0].max()+2, dtype=int)
    table = pd.DataFrame(alignments, columns=Cols, index=Rows)
    table = table.div(read_num)*100
    table.to_csv(fileout)


if __name__ == "__main__":
    main()
