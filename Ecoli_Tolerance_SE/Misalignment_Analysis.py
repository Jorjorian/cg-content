# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 13:54:32 2017

@author: ajorjorian
"""
import pandas as pd
import numpy as np
import sys


def main():
    if sys.argv[0] != '':
        filein = sys.argv[1]
        fileout = sys.argv[2]
        print(filein)
    else:
        filein = "Mem_SE/Bwa_Misreads.txt"
        fileout = "Misalignment_Table.xlsx"
        read_num = 517000
    Misalignments = pd.read_csv(filein, header=None)
    Header = Misalignments.iloc[0, 0]
    Header=Header.replace(' ','_')+ '_' + fileout
    Threads = pd.Series(Header.split("_"))
    Threads = Threads[Threads.str.isnumeric()].astype(int).values
    Misalignments = Misalignments.iloc[1:]
    Mat_Pos = np.zeros([len(Misalignments), 3])
    for i in range(len(Misalignments)):
        String = Misalignments.iloc[i, 0]
        String = String.split('_')
        String = pd.Series(String)
        String = String[String.str.isnumeric()].astype(int)
        Mat_Pos[i, :] = String.values.astype(int)
    alignments = np.zeros([2,int(max(Mat_Pos[:, 0]))-19,
                           int(max(Mat_Pos[:, 1]))+1])
    read_num = Mat_Pos[0, 2]
    for i in range(len(Mat_Pos)-1):
        Bases = int(Mat_Pos[i, 0] - 20)
        Errors = int(Mat_Pos[i, 1])
        alignments[0, Bases, Errors] = Misalignments.iloc[i, 1]
    Cols = np.arange(Mat_Pos[:, 1].min(),  Mat_Pos[:, 1].max()+1, dtype=int)
    Rows = np.arange(Mat_Pos[:, 0].min(), Mat_Pos[:, 0].max()+1, dtype=int)
    table = pd.DataFrame(alignments[0,:,:], columns=Cols, index=Rows)
    table = table.div(read_num)*100
    table.to_excel(Header)


if __name__ == "__main__":
    main()
