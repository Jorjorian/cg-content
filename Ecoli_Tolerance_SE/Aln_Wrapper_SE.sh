#!/bin/sh
N=$4
mkdir $2
task1(){
if [ -e $2/${1%%.*}.sam ] 
then
	echo "$1 exists"
else 
	a=$(basename $1);
	SECONDS=0;
	eval $3 "$1" > ${2}/${a%%.*}.sai ;
	echo "$1,$SECONDS" >> ${2}/Alignment_Timings.txt;
fi 
 }
task2(){
a=$(basename $1);
if [ -e ${1%%.*}.sam ]
then
	echo "${1%%.*}.sam exists"
else 
	bwa samse e_coli_ATCC_11303.fa "$1" ${a%%.*}.fq.gz > ${1%%.*}.sam ; 
fi 
} 
echo "command $3," >> ${2}/Alignment_Timings.txt
for file in $1*.gz; do	
	((i=i%N)); ((i++==0)) && wait 
	task1 "$file" "$2" "$3" &  
	
done
for file in ${2}/*.sai ; do 
	((i=i%N)); ((i++==0)) && wait
	task2 "$file" &
done
echo "command $3," > ${2}/Bwa_Misreads.txt
for file in $2/*.sam ; do 
	A=$(samtools view -c -F 4 "$file") 
	echo "${file},${A}" >> ${2}/Bwa_Misreads.txt
done
paste -d, ${2}/Bwa_Misreads.txt <(cut -d, f 2 ${2}/Alignment_Timing.txt)> ${2}/Alignment_Data.txt 
python3 Misalignment_Analysis.py ${2}/Alignment_Data.txt ${2}/Misalignment_Table.xlsx  
